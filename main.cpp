#include <memory>

namespace units {
  struct s {};
  struct m {};
  struct m_s {};

  template<typename TLhs, typename TRhs>
  struct multiply {};
  
  template<typename TLhs, typename TRhs>
  using multiply_t = typename multiply<TLhs, TRhs>::type;
  
  template<> struct multiply<m_s, s> { using type = m; };
}


template<typename TValue, typename TUnit>
class Quantity{
public:
  // Provides knowledge about the underlying type to the outside.
  using value_type = TValue;

  Quantity(TValue&& value) : myValue(std::forward<TValue>(value)) {}

  // Allows for static_cast<TValue>(Quantity) to get hold of value
  explicit operator TValue() const { return myValue; }

  bool operator<(Quantity const& other) { return myValue < other.myValue; }
  Quantity operator+(Quantity const& other) { return Quantity(myValue + other.myValue); }
  
private: 
  TValue myValue;
};

template<typename TVal, typename ULhs, typename URhs>
inline Quantity<TVal, units::multiply_t<ULhs, URhs>>
operator*(Quantity<TVal, ULhs> const& lhs, Quantity<TVal, URhs> const& rhs) {
  return Quantity<TVal, units::multiply_t<ULhs, URhs>>(
    static_cast<TVal>(lhs) * static_cast<TVal>(rhs)
  );
}

template<typename TVal, typename ULhs>
inline Quantity<TVal, ULhs>
operator*(Quantity<TVal, ULhs> const& lhs, Quantity<TVal, void> const& rhs) {
  return Quantity<TVal, ULhs>(static_cast<TVal>(lhs) * static_cast<TVal>(rhs));
}


template<typename TVal, typename URhs>
inline Quantity<TVal, URhs>
operator*(Quantity<TVal, void> const& lhs, Quantity<TVal, URhs> const& rhs) {
  return Quantity<TVal, URhs>(static_cast<TVal>(lhs) * static_cast<TVal>(rhs));
}


template<typename TValue>
struct Time {
  using s = Quantity<TValue, units::s>;
};

template<typename TValue>
struct Length {
  using m = Quantity<TValue, units::m>;
};

template<typename TValue>
struct Speed {
  using m_s = Quantity<TValue, units::m_s>;
};


namespace f64 {
  using value_type = double;
  using Plain = Quantity<value_type, void>;
  using Time = Time<value_type>;
  using Length = Length<value_type>;
  using Speed = Speed<value_type>;
}
using f64_t = f64::value_type;


namespace f32 {
  using value_type = float;
  using Plain = Quantity<value_type, void>;
  using Time = Time<value_type>;
  using Length = Length<value_type>;
  using Speed = Speed<value_type>;
}
using f32_t = f32::value_type;



int main() {

  // ==========================================
  // Things that should work
  f64::Plain factor(2.6);     // Ok. We can difine dimensionless "quantities" 
  f64::Speed::m_s speed(3.2); // Ok. We can define a speed.
  f64::Time::s duration(23);  // Ok. We can define a duration.
  
  f64::Speed::m_s hiSpeed(3.2);
  f64::Speed::m_s lowSpeed(1.2);
  f64::Speed::m_s sum = hiSpeed + lowSpeed; // Ok. Can add quantities of same dimension and unit.
  bool isLess = lowSpeed < hiSpeed;         // Ok. Can compare quantities of same dimension and unit


  f64::Length::m distance = speed * duration; // Ok. Should get a distance as expected.
  f64::Speed::m_s faster = speed * factor;    // Ok. Scale by dimensionless amount.
  
  f32::Time::s loresTime(2.3);
  f64::Time::s hiresTime(3.3);

  // ==========================================
  // Things that should generate compile time errors
  f32::Time::s longer = loresTime * factor; // Error. Different underlying types.
  isLess = loresTime < hiresTime;     // Error. Different underlying types.
  isLess = speed < duration;          // Error. Different quantities

  auto strange = duration * distance; // Error. Undefined result unit.
  auto odd = duration + distance;     // Error. Different dimensions.
}
